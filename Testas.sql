CREATE SCHEMA Testas;

use Testas;

drop table Statusai;
drop table Zanrai;
drop table Leidiniai;
drop table Autoriai;
drop table LeidinioAutoriai;
drop table Naudotojai;
drop table Zurnalas;
drop table Atsiliepimai;

create table Statusai (
	StatusoID int primary key auto_increment,
	Pavadinimas varchar (30)
);

create table Zanrai (
	ZanroID int primary key auto_increment,
    Pavadinimas varchar (20)
);
	ZanroID int primary key auto_increment,
    Pavadinimas varchar (30)
);

create table Leidiniai (
	LeidinioID int primary key auto_increment,
    Isleidimodata varchar (30),
    ZanroID int,
    StatutoID int,
    foreign key (ZanroID) references Zanrai (ZanroID),
    foreign key (StatutoID) references Statutai (StatutoID)
);

create table Autoriai (
	AutoriausID int primary key auto_increment,
    Vardas varchar (30)
);

create table LeidinioAutoriai (
	LeidinioID int,
    AutoriausID int,
	foreign key (LeidinioID) references Leidiniai (LeidinioID),
    foreign key (AktoriausID) references Aktoriai (AktoriausID)
);

create table Naudototjai (
	NaudotojoID int primary key auto_increment,
    Vardas varchar (20)
);

create table Zurnalas (
	ZurnaloID int primary key auto_increment,
    NaudotojoID int,
    LeidinioID int,
    foreign key (NaudotojoID) references Naudotojai (NaudotojoID),
    foreign key (LeidinioID) references Leidiniai (LeidinioID),
    PriemimoData varchar (20),
    GrazinimoData varchar (20)
);

create table Atsiliepimai (
	AtsiliepimoID int primary key auto_increment,
    Tekstas varchar (300),
    Reitingas varchar (50),
    Dataa varchar (10),
    NaudotojoID int,
    foreign key (NaudotojoID) references Naudotojai(NaudotojoID)
);

/* Uzduotys */

/* 1 */

insert into Leidiniai(Isleidimodata)
values
('2010'),
('2011'),
('2012'),
('2013'),
('2014'),
('2015'),
('2016'),
('2017'),
('2018');

select * from Leidiniai
where Isleidimodata <= ('2016');

/* 2 */

insert into Atsiliepimai(Reitingas)
values
('5.0'),
('2.8'),
('3.4'),
('1.9'),
('3.2'),
('4.5'),
('3.3'),
('1.5'),
('4.8');

select * from Atsiliepimai.Reitingas
where Reitingas > ('3.3');

/* 3 */

insert into Naudotojai(Vardas)
values
('Antanas'),
('Petras'),
('Julius'),
('Nerijus'),
('Naglis'),
('Austeja'),
('Algimantas'),
('Vladas'),
('Albanas');

select * from Naudotojai.Vardas
where Vardas ('A%_b%s');

/* 4 */

insert into Statusai(StatusoID)
values
('Laisva'),
('Drausta'),
('Drausta'),
('Laisva'),
('Laisva'),
('Laisva'),
('Drausta'),
('Drausta'),
('Drausta');

select * from Statusai.StatusoID
where StatusoID = ('Laisva');

/* 5 */

select * from LeidinioAutoriai.LeidinioID, LeidinioAutoriai.AutoriausID;